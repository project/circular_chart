<?php
/**
 * @file
 * Circular Chart template file.
 *
 * Stores the HTML to generate a chart.
 */
?>
<div <?php print drupal_attributes($chart['#attributes']); ?>></div>
