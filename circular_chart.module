<?php
/**
 * @file
 * Contains the hooks for the circular chart module.
 */

/**
 * The includes of the field HOOKs related file.
 */
require_once 'includes/circular_chart.field.inc';

/**
 * Implements hook_help().
 */
function circular_chart_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/help#circular_chart':
      $output .= '<p>' . t('The %mod will provide circular graphs based on !url library', array(
        '%mod' => 'Circular Chart',
        '!url' => l(t('circles'), 'https://github.com/lugolabs/circles', array('external' => TRUE)),
      )) . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_permission().
 */
function circular_chart_permission() {
  return array(
    'administer circular_chart settings' => array(
      'title' => t('Administer Circular Chart Presets'),
      'description' => t('Administer Circular Chart Presets'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function circular_chart_theme($existing, $type, $theme, $path) {
  return array(
    'circle_chart' => array(
      'variables' => array('chart' => array()),
      'path' => drupal_get_path('module', 'circular_chart') . '/templates',
      'template' => 'circular-chart-view',
    ),
  );
}

/**
 * Implements hook_ctools_plugin_api().
 */
function circular_chart_ctools_plugin_api($owner, $api) {
  if ($owner == 'circular_chart' && $api == 'default_circular_chart_environments') {
    return array(
      'version' => 1,
      'file' => 'includes/circular_chart.default_export.inc',
    );
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function circular_chart_ctools_plugin_directory($module, $type) {
  // Load the export_ui plugin.
  if ($type == 'export_ui') {
    return 'plugins/export_ui';
  }
}

/**
 * Implements hook_libraries_info().
 */
function circular_chart_libraries_info() {
  $libraries = array();
  $libraries['circles'] = array(
    'title' => 'lugolabs-circles',
    'vendor url' => 'http://lugolabs.com/circles',
    'download url' => 'https://github.com/lugolabs/circles/zipball/master',
    'version arguments' => array(
      'file' => 'circles.min.js',
      'pattern' => '/circles - v(\d+\.+\d+)/',
      'lines' => 2,
    ),
    'files' => array(
      'js' => array('circles.min.js'),
    ),
  );

  return $libraries;
}

/**
 * Form API to build a Preset add/edit form.
 */
function circular_chart_preset_config(&$form, &$form_state) {
  $lib_name = 'circles';
  // Show libraries error if the circles.js are not found.
  $library = libraries_detect($lib_name);
  if (empty($library['installed'])) {
    drupal_set_message(t('!msg', array('!msg' => $library['error message'])), 'error');
  }
  // Add required JavaScript.
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'circular_chart') . '/js/circular_chart.js',
  );
  $circular_chart = $form_state['item'];

  // Check for the stored serialized data.
  $circular_chart->circle_radius = '';
  $circular_chart->circle_width = '';
  $circular_chart->circle_bg_color = '';
  $circular_chart->circle_fg_color = '';
  $circular_chart->circle_anim_duration = '';
  $circular_chart->circle_wrapper_class = '';
  if (!empty($circular_chart->data)) {
    $data = $circular_chart->data;
    $circular_chart->circle_radius = $data['circle_radius'];
    $circular_chart->circle_width = $data['circle_width'];
    $circular_chart->circle_bg_color = $data['circle_bg_color'];
    $circular_chart->circle_fg_color = $data['circle_fg_color'];
    $circular_chart->circle_anim_duration = $data['circle_anim_duration'];
    $circular_chart->circle_wrapper_class = $data['circle_wrapper_class'];
  }
  unset($form['info']);

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name for the preset.'),
    '#default_value' => $circular_chart->label,
    '#id' => 'label',
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#default_value' => $circular_chart->name,
    '#maxlength' => 32,
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'circular_chart_machine_name_available',
      'source' => array('label'),
    ),
    '#id' => 'machine-name',
  );

  $form['circle_radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Circle radius'),
    '#description' => t('The radius of the circle to be generated.'),
    '#default_value' => (!empty($circular_chart->circle_radius) ? $circular_chart->circle_radius : 60),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['circle_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Circle width'),
    '#description' => t('The radius of the circle to be generated.'),
    '#default_value' => (!empty($circular_chart->circle_width) ? $circular_chart->circle_width : 10),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['circle_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Circle background colour'),
    '#description' => t('This will be used to color the full circle as a background colour.'),
    '#default_value' => (!empty($circular_chart->circle_bg_color) ? $circular_chart->circle_bg_color : '#EEEEEE'),
    '#id' => 'circle-bg-color',
    '#attributes' => array(
      'class' => array('edit-circle-chart-colorpicker'),
    ),
    '#attached' => array(
      'library' => array(
        array('system', 'farbtastic'),
      ),
    ),
    '#suffix' => '<div class="circle-chart-colorpicker"></div>',
  );

  $form['circle_fg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Circle foreground colour'),
    '#description' => t('This will be used to color the full circle as a foreground colour.'),
    '#default_value' => (!empty($circular_chart->circle_fg_color) ? $circular_chart->circle_fg_color : '#FF0000'),
    '#id' => 'circle-fg-color',
    '#attributes' => array(
      'class' => array('edit-circle-chart-colorpicker'),
    ),
    '#attached' => array(
      'library' => array(
        array('system', 'farbtastic'),
      ),
    ),
    '#suffix' => '<div class="circle-chart-colorpicker"></div>',
  );

  $form['circle_anim_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Circle Animation Duration'),
    '#description' => t('The animation time to generate the full chart.'),
    '#default_value' => (!empty($circular_chart->circle_anim_duration) ? $circular_chart->circle_anim_duration : 500),
  );

  $form['circle_wrapper_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Circle Wrapper Class'),
    '#description' => t('The wrapper class which will wrap the full circular chart.'),
    '#default_value' => (!empty($circular_chart->circle_wrapper_class) ? $circular_chart->circle_wrapper_class : 'circles-wrp'),
  );

  return $form;
}

/**
 * Validate function to validate the form inputs.
 *
 * @see circular_chart_preset_config()
 */
function circular_chart_preset_config_validate(&$form, &$form_state) {
  $color_elements = array('circle_bg_color', 'circle_fg_color');
  $values = $form_state['values'];
  foreach ($color_elements as $color_element) {
    // Validate that the colors are in valid Hex format.
    if (!preg_match('/^#[a-f0-9]{6}$/i', $values[$color_element])) {
      form_set_error($color_element, t('%name must be a valid color in the format <em>#RRGGBB</em>.', array('%name' => $form[$color_element]['#title'])));
    }
  }
}

/**
 * Submit handler for the Chat presets.
 *
 * @see circular_chart_preset_config()
 */
function circular_chart_preset_config_submit(&$form, &$form_state) {
  $form_state['item']->data = array(
    'circle_radius' => $form_state['values']['circle_radius'],
    'circle_width' => $form_state['values']['circle_width'],
    'circle_bg_color' => $form_state['values']['circle_bg_color'],
    'circle_fg_color' => $form_state['values']['circle_fg_color'],
    'circle_anim_duration' => $form_state['values']['circle_anim_duration'],
    'circle_wrapper_class' => $form_state['values']['circle_wrapper_class'],
  );
}

/**
 * Function to check unique machine name.
 *
 * @param string $machine
 *   - The machine name stored in files or in database.
 *
 * @return int
 *   - Integer returning the count from the database table.
 */
function circular_chart_machine_name_available($machine) {
  $result = db_select('circular_chart_preset', 'cap')
    ->fields('cap', array('name'))
    ->condition('name', $machine, '=')
    ->execute()->rowCount();
  return $result;
}

/**
 * Function to load a single or multiple chart presets based on the argument.
 *
 * @param array $names
 *   - Argument as an array which provides the machine names of the presets.
 *
 * @return object
 *   - Object populated with chart presets.
 */
function circular_chart_load_chart($names = array()) {
  if (!empty($names)) {
    return _circular_chart_load('names', $names);
  }
  return array();
}

/**
 * Function to load all the circular chart presets.
 *
 * @return object
 *   - Object populated with chart presets.
 */
function circular_chart_load_all_charts() {
  return _circular_chart_load();
}

/**
 * Helper function to load the data from ctools cache or database table.
 *
 * This function should not be used directly.
 *
 * @param string $type
 *   - This supplies the type of data needed to be fetched. Depends on the
 *     ctools_export_load_object() function's parameters.
 * @param array $args
 *   - If the $type parameter is mentioned other than "all", then should be
 *     passed as an array. Same as ctools_export_load_object() function's
 *     parameter.
 *
 * @return object
 *   - An object containing ctools cache or database table data.
 */
function _circular_chart_load($type = 'all', $args = array()) {
  $result = array();
  ctools_include('export');
  switch ($type) {
    case 'all':
      $result = ctools_export_load_object('circular_chart_preset', 'all', array());
      break;

    case 'names':
      $result = ctools_export_load_object('circular_chart_preset', 'names', $args);
      break;
  }
  return $result;
}
